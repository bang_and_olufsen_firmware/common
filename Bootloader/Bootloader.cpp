#include "Bootloader.hpp"

Bootloader::Bootloader(std::function<void()> callback) : setJumpTime(callback)
{
  flash.init();
  programBuffer.reserve(flash.get_page_size() * 2);
  compute_application_crc();
  valid = get_application_crc() == get_crc_from_flash();
#ifdef DEBUG_PRINT
  printf("CRCAPP: 0x%08lX, FLASH: 0x%08lX\n", get_application_crc(), get_crc_from_flash());
#endif
  if(!valid)
  {
    restart();
  }
}

void Bootloader::compute_application_crc()
{
  crcEngine.compute((void*)startAddress, get_application_size() - 4, &CRC);
}

void Bootloader::restart()
{
#ifdef DEBUG_PRINT
  printf("Restarting boot process!\n");
#endif
  programBuffer.clear();

  valid    = false;
  progress = startAddress;
}

void Bootloader::flush_buffer()
{
  if((progress % flash.get_sector_size(progress)) == 0)
  {
    flash.erase(progress, flash.get_sector_size(progress));
  }
  flash.program(programBuffer.data(), progress, flash.get_page_size());
  progress += flash.get_page_size();
  programBuffer.erase(programBuffer.begin(), programBuffer.begin() + flash.get_page_size());
}

void Bootloader::jump()
{
  Watchdog::get_instance().stop();
  mbed_start_application(startAddress);
}

bool Bootloader::addData(uint32_t address, const uint8_t* data, uint32_t length, uint8_t repeat)
{
  if(address != getProgress())
  {
    return false;
  }

  valid = false;

  for(uint16_t r = 0; r < repeat; r++)
  {
    for(uint32_t i = 0; i < length; i++)
    {
      programBuffer.push_back(data[i]);
      if(should_flush_buffer())
      {
        flush_buffer();
        if(isDone())
        {
          compute_application_crc();
          valid = get_application_crc() == get_crc_from_flash();
#ifdef DEBUG_PRINT
          printf("CRCAPP: 0x%08lX, FLASH: 0x%08lX\n", get_application_crc(), get_crc_from_flash());
#endif
          valid ? setJumpTime() : restart();
          return true;
        }
      }
    }
  }
  return true;
}

bool Bootloader::should_flush_buffer()
{
  return programBuffer.size() >= flash.get_page_size() && progress < endAddress;
}