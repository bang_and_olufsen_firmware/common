#pragma once
#include <FlashIAP.h>
#include <stdint.h>

#include "common/AppTypes.h"
#include "mbed.h"
#include <functional>
#include <vector>

class Bootloader {
 public:
  Bootloader(std::function<void()> callback);
  ~Bootloader() = default;

  bool                      addData(uint32_t address, const uint8_t* data, uint32_t length, uint8_t repeat);
  void                      restart();
  bool                      isValid() { return valid; }
  bool                      isDone() { return progress >= endAddress; }
  uint32_t                  getProgress() { return progress + programBuffer.size(); }
  void                      jump();
  static constexpr uint32_t get_crc_from_flash() { return *(uint32_t*)(endAddress - 4); }
  uint32_t                  get_application_crc() { return CRC; }

 public:
  void                      compute_application_crc();
  static constexpr uint32_t get_application_size() { return endAddress - startAddress; }
  bool                      should_flush_buffer();
  void                      flush_buffer();
  static uint32_t           getBootloaderUpdaterCRC() { return *((uint32_t*)(0x1fff0 - 4)); }
  static uint32_t           getBootloaderCRC() { return *((uint32_t*)(0xA000 - 4)); }
  static AppType            getApptype() { return static_cast<AppType>(*((uint8_t*)(0x20000 - 5))); }

  std::function<void()> setJumpTime;

#if defined(BOOTUPDATER_STAND) || defined(BOOTUPDATER_SPEAKER)
  static constexpr uint32_t startAddress = 0x00000;
  static constexpr uint32_t endAddress   = 0x0A000;
#else
  static constexpr uint32_t startAddress = 0x0A000;
  static constexpr uint32_t endAddress   = 0x20000;
#endif
  bool                                         valid    = false;
  uint32_t                                     progress = startAddress;
  uint32_t                                     CRC      = 0;
  mbed::FlashIAP                               flash;
  MbedCRC<POLY_32BIT_ANSI, 32, CrcMode::TABLE> crcEngine;
  std::vector<uint8_t>                         programBuffer;
};