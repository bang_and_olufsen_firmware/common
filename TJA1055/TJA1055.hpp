#pragma once

#include "common/CANMessages.hpp"
#include "mbed.h"
#include <array>

class TJA1055 : public CAN {
  typedef void (*messageCallback)(CANMessage&);

 public:
  TJA1055(PinName rx, PinName tx, PinName err, PinName stb, PinName enable);
  ~TJA1055() = default;
  void registerCallback(CanMessageType msgType, messageCallback callback);
  void recieveMessages();

 private:
  DigitalIn                        err;
  DigitalOut                       stb;
  DigitalOut                       enable;
  std::array<messageCallback, 256> callbacks = {nullptr};
};