#include "TJA1055.hpp"

TJA1055::TJA1055(PinName rx, PinName tx, PinName err, PinName stb, PinName enable)
  : CAN(rx, tx, 125000),
    err(err),
    stb(stb, 1),
    enable(enable, 1)
{
}

void TJA1055::registerCallback(CanMessageType msgType, TJA1055::messageCallback callback)
{
  callbacks[static_cast<int>(msgType)] = callback;
}

void TJA1055::recieveMessages()
{
  CANMessage msg;
  while(read(msg) != 0)
  {
    auto callback = msg.id < callbacks.size() ? callbacks[msg.id] : nullptr;
    if(callback == nullptr)
    {
      continue;
    }
    callback(msg);
  }
}
