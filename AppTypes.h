#pragma once

enum class AppType {
  MAINAPP            = 0x11,
  BOOTLOADER         = 0x22,
  BOOTLOADER_UPDATER = 0x33,
};