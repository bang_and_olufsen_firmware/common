#pragma once
#include <array>
#include <numeric>

template <typename T, uint8_t N>
class MovingAverageBase {
 public:
  MovingAverageBase()  = default;
  ~MovingAverageBase() = default;
  T update(T val)
  {
    buffer[(index++) % buffer.size()] = val;
    if(samples < buffer.size())
    {
      samples++;
    }
    last = getAvg();
    return last;
  }
  T getSum() const
  {
    if(samples == 1 << N)
    {
      return std::accumulate(buffer.cbegin(), buffer.cend(), T());
    }
    else
    {
      return std::accumulate(buffer.cbegin(), buffer.cbegin() + samples, T());
    }
  }
  virtual T getAvg() const = 0;
  T         getLast() const { return last; }

 protected:
  uint32_t              samples = 0;
  uint32_t              index   = 0;
  T                     last    = 0;
  std::array<T, 1 << N> buffer;
};

template <typename T, uint8_t N>
class MovingAverage : public MovingAverageBase<T, N> {
 public:
  virtual T getAvg() const
  {
    if(this->samples == 1 << N)
    {
      return this->getSum() / (1 << N);
    }
    else
    {
      return this->getSum() / this->samples;
    }
  }
};

template <typename T, uint8_t N>
class MovingAverageF : public MovingAverageBase<T, N> {
 public:
  virtual T getAvg() const
  {
    if(this->samples == 1 << N)
    {
      return this->getSum() / float(1 << N);
    }
    else
    {
      return this->getSum() / float(this->samples);
    }
  }
};
