#pragma once
#include <algorithm>
#include <array>

template <typename T, uint32_t N>
class Median {
 public:
  Median()  = default;
  ~Median() = default;
  void update(T val)
  {
    buffer[(index++) % buffer.size()] = val;
    if(samples < buffer.size())
    {
      samples++;
    }

    median = getMedian();
  }

  T median = 0;

  bool ready() { return samples >= buffer.size(); }

 private:
  uint32_t         samples = 0;
  uint32_t         index   = 0;
  std::array<T, N> buffer;

  T getMedian() const
  {
    std::array<T, N> tmp = buffer;
    uint32_t         n   = tmp.size() / 2;
    std::nth_element(tmp.begin(), tmp.begin() + n, tmp.end());
    if(tmp.size() % 2 == 1)
    {
      return tmp[n];
    }
    else
    {
      std::nth_element(tmp.begin(), tmp.begin() + n + 1, tmp.end());
      return (tmp[n] + tmp[n + 1]) * 0.5f;
    }
  }
};