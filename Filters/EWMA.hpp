#pragma once

class EWMA {
 public:
  EWMA(float alpha) : alpha(alpha) {}
  ~EWMA() = default;

  float update(float input)
  {
    output = alpha * (input - output) + output;
    return output;
  }

  float updateWithAlphas(float input, float alphaUp, float alphaDown)
  {
    if(input > output)
    {
      output = alphaUp * (input - output) + output;
    }
    else
    {
      output = alphaDown * (input - output) + output;
    }
    return output;
  }

  void  reset() { output = 0; }
  float output;
  float alpha;
};