#pragma once
#include "common/CANMessages.hpp"
#include "common/CPULoadTimer/CPULoadTimer.hpp"
#include "common/TJA1055/TJA1055.hpp"

namespace DeviceStats
{
void sendHeap(TJA1055& bus, CanMessageType type);
void sendUptime(TJA1055& bus, CanMessageType type);
void sendLoad(TJA1055& bus, CPULoadTimer& timer, CanMessageType type);

}  // namespace DeviceStats