#include "DeviceStats.hpp"

#include "mbed.h"

namespace DeviceStats
{
void sendUptime(TJA1055& bus, CanMessageType type)
{
  uint8_t data[8];
  *(reinterpret_cast<uint64_t*>(data + 0)) = __builtin_bswap64(get_ms_count());

  bus.write(mbed::CANMessage(static_cast<unsigned int>(type), data, sizeof(data)));
}

void sendLoad(TJA1055& bus, CPULoadTimer& timer, CanMessageType type)
{
  static uint64_t lastTime  = 0;
  const uint64_t  fullTimer = timer.getFullTimer();
  const uint64_t  loadTime  = timer.getLoadTime();
  const uint64_t  diffTime  = fullTimer - lastTime;

  timer.resetLoad();
  lastTime = fullTimer;

  uint8_t data[4];
  if(diffTime)
  {
    *(reinterpret_cast<float*>(data + 0)) = loadTime / (float)(diffTime / 100.f);
  }
  else
  {
    *(reinterpret_cast<float*>(data + 0)) = -1;
  }
  bus.write(mbed::CANMessage(static_cast<unsigned int>(type), data, sizeof(data)));
}

void sendHeap(TJA1055& bus, CanMessageType type)
{
  mbed_stats_heap_t stats;
  mbed_stats_heap_get(&stats);
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data + 0)) = __builtin_bswap32(stats.total_size);
  *(reinterpret_cast<uint32_t*>(data + 4)) = __builtin_bswap32(stats.max_size);

  bus.write(mbed::CANMessage(static_cast<unsigned int>(type), data, sizeof(data)));
}

}  // namespace DeviceStats