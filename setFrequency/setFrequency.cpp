#include "setFrequency.hpp"

#include "LPC17xx.h"
#include "mbed.h"

typedef enum {
  LPC1769 = 0x26113F37,
  LPC1768 = 0x26013F37,
  LPC1767 = 0x26012837,
  LPC1766 = 0x26013F33,
  LPC1765 = 0x26013733,
  LPC1764 = 0x26011922,
  LPC1763 = 0x26012033,
  LPC1759 = 0x25113737,
  LPC1758 = 0x25013F37,
  LPC1756 = 0x25011723,
  LPC1754 = 0x25011722,
  LPC1752 = 0x25001121,
  LPC1751 = 0x25001118,
} lpc_part_id_t;

#define IAP_LOCATION 0x1FFF1FF1

typedef void (*IAP)(unsigned long[], unsigned long[]);
IAP iap_entry = (IAP)IAP_LOCATION;

void pllfeed()
{
  __disable_irq();
  LPC_SC->PLL0FEED = 0x000000aa;
  LPC_SC->PLL0FEED = 0x00000055;
  __enable_irq();
}

int get_part_id(void)
{
  ScopedRamExecutionLock make_ram_executable;
  ScopedRomWriteLock     make_rom_writable;

  unsigned long command[5] = {0, 0, 0, 0, 0};
  unsigned long result[5]  = {0, 0, 0, 0, 0};
  command[0]               = 54;  // partID
  iap_entry(command, result);
  return result[1];
}

void setFrequency()
{
  {
    if(get_part_id() == LPC1768)
    {
      return;
    }
  }
  // the crystal oscillator is 16 MHz
  // main oscillator frequency 300 MHz: M = (300 x N) / (2 x 16)
  uint32_t n = 8;
  uint32_t m = 72;
  // processor clock 100 MHz = 300 MHz / D
  uint32_t d = 3;
  // disconnect
  LPC_SC->PLL0CON = 0x00000001;
  pllfeed();
  // disable
  LPC_SC->PLL0CON = 0x00000000;
  pllfeed();
  // set new PLL values
  LPC_SC->PLL0CFG = ((n - 1) << 16) | (m - 1);
  pllfeed();
  // enable
  LPC_SC->PLL0CON = 0x00000001;
  pllfeed();
  // set cpu clock divider
  LPC_SC->CCLKCFG = d - 1;
  // wait for lock
  while((LPC_SC->PLL0STAT & 0x04000000) == 0)
    ;
  // connect
  LPC_SC->PLL0CON = 0x00000003;
  pllfeed();
}
