#pragma once

enum class PCBType {
  STAND_CONTROLLER,
  SPEAKER_CONTROLLER,
};