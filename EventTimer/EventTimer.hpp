#pragma once
#include "mbed.h"
#include <functional>
#include <vector>

class EventTimer {
 public:
  EventTimer() = default;
  void addEvent(std::function<void(void)> callback, uint64_t first_publish, uint16_t interval)
  {
    events.push_back({callback, first_publish, interval});
  }
  void step()
  {
    uint64_t time = get_ms_count();
    for(auto& event : events)
    {
      if(event.next_publish < time)
      {
        event.callback();
        event.next_publish += event.interval;
      }
    }
  }

 private:
  struct Event {
    std::function<void(void)> callback;
    uint64_t                  next_publish;
    uint16_t                  interval;
  };
  std::vector<Event> events;
};