#include "PinStateReader.hpp"

namespace pinstate
{
void PinStateReader::service()
{
  switch(readState)
  {
    case ReadState::PULL_DOWN:
      stateDown = pin.read();
      pin.mode(PullUp);
      readState = ReadState::PULL_UP;
      break;
    case ReadState::PULL_UP:
      stateUp = pin.read();
      pin.mode(PullDown);
      readState = ReadState::PULL_DOWN;
      break;
  }
}

PinState PinStateReader::get()
{
  if(stateDown != stateUp)
  {
    return PinState::FLOATING;
  }
  if(stateDown == true)
  {
    return PinState::HIGH;
  }
  return PinState::LOW;
}
}  // namespace pinstate