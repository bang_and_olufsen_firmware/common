#pragma once

#include "mbed.h"

namespace pinstate
{
enum class PinState {
  LOW,
  HIGH,
  FLOATING,
};

enum class ReadState {
  PULL_DOWN,
  PULL_UP,
};

class PinStateReader {
 public:
  PinStateReader(PinName _pin) : pin(_pin, PullDown){};

  PinState get();
  void     service();

 private:
  ReadState readState = ReadState::PULL_DOWN;
  DigitalIn pin;
  bool      stateDown = false;
  bool      stateUp   = true;
};
}  // namespace pinstate