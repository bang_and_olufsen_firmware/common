#pragma once
#include <string>

namespace speakermotor
{

enum class BOSpeakerControlTarget {
  UNFOLD,
  FOLD_IN,
  STOP,
  UNKNOWN,
};

inline std::string BOSpeakerControlTarget_str(const BOSpeakerControlTarget& x)
{
  switch(x)
  {
    case BOSpeakerControlTarget::UNFOLD:
      return "UNFOLD";
    case BOSpeakerControlTarget::FOLD_IN:
      return "FOLD_IN";
    case BOSpeakerControlTarget::STOP:
      return "STOP";
    default:
      return "UNKNOWN";
  }
}

inline BOSpeakerControlTarget str_BOSpeakerControlTarget(const std::string& x)
{
  if(x == "UNFOLD")
  {
    return BOSpeakerControlTarget::UNFOLD;
  }
  else if(x == "FOLD_IN")
  {
    return BOSpeakerControlTarget::FOLD_IN;
  }
  else if(x == "STOP")
  {
    return BOSpeakerControlTarget::STOP;
  }
  else
  {
    return BOSpeakerControlTarget::UNKNOWN;
  }
}

enum class BOSpeakerControlState {
  UNFOLDED,
  FOLDED,
  UNFOLDING,
  FOLDING,
  STOPPED,
  UNKNOWN,
  CNT,
};

inline std::string BOSpeakerControlState_str(const BOSpeakerControlState& x)
{
  switch(x)
  {
    case BOSpeakerControlState::UNFOLDED:
      return "UNFOLDED";
    case BOSpeakerControlState::FOLDED:
      return "FOLDED";
    case BOSpeakerControlState::UNFOLDING:
      return "UNFOLDING";
    case BOSpeakerControlState::FOLDING:
      return "FOLDING";
    case BOSpeakerControlState::STOPPED:
      return "STOPPED";
    default:
      return "UNKNOWN";
  }
}

}  // namespace speakermotor