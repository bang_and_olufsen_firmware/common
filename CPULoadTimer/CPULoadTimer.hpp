#pragma once
#include "LPC17xx.h"

class CPULoadTimer {
 public:
  CPULoadTimer(LPC_TIM_TypeDef* timer) : timer(timer)
  {
    timer->TCR = 2;           // Disable and reset timer
    timer->PR  = 0xFFFFFFFF;  // Use full prescaler

    // Set count source as PCLK, not scaled
    volatile uint32_t* clksel     = &LPC_SC->PCLKSEL0;
    uint32_t           pclk_shift = 2;
    if(timer == LPC_TIM1)
    {
      pclk_shift = 4;
    }
    else if(timer == LPC_TIM2)
    {
      clksel     = &LPC_SC->PCLKSEL1;
      pclk_shift = 12;
    }
    else if(timer == LPC_TIM3)
    {
      clksel     = &LPC_SC->PCLKSEL1;
      pclk_shift = 14;
    }

    *clksel &= ~(3 << pclk_shift);
    *clksel |= 1 << pclk_shift;

    timer->CTCR &= (~3U);
    timer->TCR = 1;  // Enable counter
  }

  uint32_t getShortTimer() const { return timer->PC; }
  uint64_t getFullTimer() const
  {
    uint32_t lower = timer->PC;
    uint32_t upper = timer->TC;
    if(lower < timer->PC)
    {
      lower = timer->PC;
      upper = timer->TC;
    }
    return (((uint64_t)upper) << 32) + (uint64_t)lower;
  }
  uint64_t getLoadTime() { return loadTime; }

  float getTimeSince(uint32_t timestamp) const
  {
    const uint32_t diff = getShortTimer() - timestamp;
    return diff * (1 / 96000000.f);
  }

  float getTimeSince(uint64_t timestamp) const
  {
    const uint64_t diff = getFullTimer() - timestamp;
    return diff * (1 / 96000000.f);
  }

  void startLoad() { startTime = getShortTimer(); }
  void endLoad() { loadTime += getShortTimer() - startTime; }
  void resetLoad() { loadTime = 0; }

  static float   toSeconds(int64_t ticks) { return ticks / 96000000.f; }
  static int64_t fromSeconds(float s) { return s * 96000000.f; }

 private:
  uint64_t         loadTime  = 0;
  uint32_t         startTime = 0;
  LPC_TIM_TypeDef* timer;
};