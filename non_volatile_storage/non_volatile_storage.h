#pragma once

#include <stdint.h>

uint32_t non_volatile_storage_get(uint32_t index);
void     non_volatile_storage_set(uint32_t index, uint32_t value);