#include "non_volatile_storage.h"

#include "LPC17xx.h"
uint32_t non_volatile_storage_get(uint32_t index)
{
  // Enable RTC clock
  LPC_SC->PCONP |= 1 << 9;

  // Set value
  return *((&LPC_RTC->GPREG0) + index);
}

void non_volatile_storage_set(uint32_t index, uint32_t value)
{
  // Enable RTC clock
  LPC_SC->PCONP |= 1 << 9;

  // Set value
  *((&LPC_RTC->GPREG0) + index) = value;
}